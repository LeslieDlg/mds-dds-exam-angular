FROM node:16.13.1-alpine3.15 AS builder
WORKDIR /app
ENV BACK_URL="https://spring-boot-exam-ldelagne.herokuapp.com/api/tutorials"
COPY package.json package-lock.json ./
RUN npm install @angular/cli -g
RUN npm install
COPY . .
RUN ng build --prod

FROM nginx:stable-alpine
COPY --from=builder /app/dist/mds-dds-exam-angular /usr/share/nginx/html
ENTRYPOINT nginx -g 'daemon off;'

