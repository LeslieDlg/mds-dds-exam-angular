## Init your env
Run `npm install -g @angular/cli`
Run `npm install`

## Development server

Run `ng serve --port 8081` for a dev server. Navigate to `http://localhost:8081/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Docker

Build `docker build -t deploy-angular-exam .`

Run `docker run -p 1234:80 --name deploy-exam-angular -d deploy-angular-exam:latest`
